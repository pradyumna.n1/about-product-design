About XPA / Jarvis
---
[Intelligent Virtual Assistants](https://en.wikipedia.org/wiki/Virtual_assistant) have existed since long. There are two problems that they do not solve
1. You cannot talk to them, and teach them something new. Thats why we are building something like [J.A.R.V.I.S.](https://ironman.fandom.com/wiki/J.A.R.V.I.S.) 
2. They cannot assist you in operating your "daily use software". Thats why we are building a [RPA](https://en.wikipedia.org/wiki/Robotic_process_automation) which would be [augmented by human workforce](https://youtu.be/1SximAg9t4w?t=788). Instead of just Robots doing Process Automation, we will have Humans as well doing Process Automation - hence instead of RPA, we will build XPA. Where X = Robot + Human

Attempts
---
We might attempt building either of these. Automating the job of..
* .. [A System Adminstrator](https://gitlab.com/xpa-jarvis/product-design/automate-system-admin)
* .. others coming soon

# User Interface
[Actors](https://en.wikipedia.org/wiki/Actor_(UML)) in our system are `Customer`, `Trainer`, `DOer` and `Admin`.
Each one of them will have the following user interface
* `Customer` would say things like "create new mailbox myname[at]mycompany.com"
* `Trainer` would make machine understand how to understand the above natural language text, and make machine understand which software to open, which screen to open, and how to operate the screen to fulfill the above request
* `DOer` would come in and operate on the screen of user if Machine does not has enough training to perform the task given by `Customer`
* `Admin` would be able to see the activity of each `DOer`., `Trainer`, `Customer`. Assume account of anyone to unblock them.

## Customer interface
put figma here, and link to use cases here.
https://figma-link-here.com/
## Trainer interface
put figma here, and link to use cases here.
## DOer interface
put figma here, and link to use cases here.
## Admin interface
put figma here, and link to use cases here.
